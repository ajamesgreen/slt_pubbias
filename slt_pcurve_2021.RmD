---
title: "SLT PCurve 2021"
author: "James Green"
date: "25/07/2021"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_knit$set(root.dir = getwd())
```

```{r file work, include = FALSE}
library(tidyverse)
source("pcurvej.R")

# need to read in coded data and tidy it up a bit
slt <- readxl::read_excel("P-Curve Disclosure Table 2021.xlsx", sheet = "extraction")
slt <- filter(slt, `Recompute Results` > 0)
slt$`Test Inputted into P-Curve` <- gsub(" ", "", slt$`Test Inputted into P-Curve`)

# need directory for pcurve files
dir1 <- getwd()
dir1 <- paste0(dir1, "/pcurve")

# need each meta separately
estes <- filter(slt, meta == "estes2007")
law <- filter(slt, meta == "law2004")
nye <- filter(slt, meta == "nye2013")
rk <- filter(slt, meta == "robertskaiser2011")

select(slt, `Test Inputted into P-Curve`) %>%
  write_delim("pcurve/overall.p.txt", col_names = FALSE)
select(estes, `Test Inputted into P-Curve`) %>%
  write_delim("pcurve/estes.p.txt", col_names = FALSE)
select(law, `Test Inputted into P-Curve`) %>%
  write_delim("pcurve/law.p.txt", col_names = FALSE)
select(nye, `Test Inputted into P-Curve`) %>%
  write_delim("pcurve/nye.p.txt", col_names = FALSE)
select(rk, `Test Inputted into P-Curve`) %>%
  write_delim("pcurve/rk.p.txt", col_names = FALSE)
```

# P-curve app

Because of the way the pcurve script writes multiple files each time it runs, p
p-curve is being run in a separate file.

Also, some adjustments have been made to the original code to return a dataframe
that contains key information for further processing. This is collated and then 
returned to the main file.

## P-curve for all studies

```{r}
slt.pc <- pcurve_app_j("overall.p.txt", dir1)
```

## Estes et al (2007)

```{r}
estes.pc <- pcurve_app_j("estes.p.txt", dir1)
```

## Law et al (2004)

```{r}
law.pc <- pcurve_app_j("law.p.txt", dir1)
```

## Nye et al (2013)

```{r}
nye.pc <- pcurve_app_j("nye.p.txt", dir1)
```

## Roberts & Kaiser (2011)

```{r}
rk.pc <- pcurve_app_j("rk.p.txt", dir1)
```
## Make some tidy dataframes

```{r}
pcurve.results <- rbind(slt.pc$pcurve_summary,
                        estes.pc$pcurve_summary,
                        law.pc$pcurve_summary,
                        nye.pc$pcurve_summary,
                        rk.pc$pcurve_summary)
pcurve.results$file <- gsub(".p.txt", "", pcurve.results$file)

pcurve.obs <- matrix(c(slt.pc$obs,
                        estes.pc$obs,
                        law.pc$obs,
                        nye.pc$obs,
                        rk.pc$obs), ncol = 5, byrow = TRUE) %>%
  as.data.frame()

names(pcurve.obs) <- c("obs.01", "obs.02", "obs.03", "obs.04", 'obs.05')
pcurve.obs$file <- pcurve.results$file

pcurve.null33 <- matrix(c(slt.pc$null33,
                        estes.pc$null33,
                        law.pc$null33,
                        nye.pc$null33,
                        rk.pc$null33), ncol = 5, byrow = TRUE) %>%
  as.data.frame()

names(pcurve.null33) <- c("null33.01", "null33.02", "null33.03", "null33.04", 'null33.05')
pcurve.null33$file <- pcurve.results$file
```


```{r output data}
save(pcurve.results, file = "pcurve.results.Rdata")
save(pcurve.obs, file = "pcurve.obs.Rdata")
save(pcurve.null33, file = "pcurve.null33.Rdata")
```

